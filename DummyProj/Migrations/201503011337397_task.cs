namespace DummyProj.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class task : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskDBs", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.TaskDBs", "Priority", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskDBs", "Priority", c => c.Int(nullable: false));
            AlterColumn("dbo.TaskDBs", "Description", c => c.String());
        }
    }
}
