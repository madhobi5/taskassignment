﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DummyProj.Models
{
    public class TaskDB
    {
        [Key]
        public int taskId { get; set; }

        [Required(ErrorMessage="*")]
        public string Title { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [Display(Name="Due Date")]
        [Required(ErrorMessage = "*")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DueDate { get; set; }

        [Display(Name="Required Hours")]
        [Range(1, 100)]
        [Required(ErrorMessage = "*")]
        public int RequiredHours { get; set; }

        [Required(ErrorMessage = "*")]
        public string Priority { get; set; }
    }

    public class TaskDbContext : DbContext
    {
        public DbSet<TaskDB> Tasks { get; set; }
    }
}