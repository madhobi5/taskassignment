﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DummyProj.Models;

namespace DummyProj.Controllers
{
    public class ChartController : Controller
    {
        private TaskDbContext db = new TaskDbContext();
        //
        // GET: /Chart/

        public ActionResult Index()
        {
            return View(BarChart());
        }

        public JsonResult BarChart()
        {
            var chartData = db.Tasks.ToList();
            return Json(chartData, JsonRequestBehavior.AllowGet);
        }
    }
}
