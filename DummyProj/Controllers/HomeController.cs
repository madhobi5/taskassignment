﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DummyProj.Models;

namespace DummyProj.Controllers
{
    public class HomeController : Controller
    {
        private TaskDbContext db = new TaskDbContext();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View(db.Tasks.ToList());
        }

        //
        // GET: /Home/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    TaskDB taskdb = db.Tasks.Find(id);
        //    if (taskdb == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(taskdb);
        //}

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskDB taskdb)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(taskdb);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(taskdb);
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            if (taskdb == null)
            {
                return HttpNotFound();
            }
            return View(taskdb);
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TaskDB taskdb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(taskdb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(taskdb);
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            if (taskdb == null)
            {
                return HttpNotFound();
            }
            return View(taskdb);
        }

        //
        // POST: /Home/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            db.Tasks.Remove(taskdb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}