﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DummyProj.Models;

namespace DummyProj.Controllers
{
    public class TaskController : Controller
    {
        private TaskDbContext db = new TaskDbContext();

        //
        // GET: /Task/

        public ActionResult Index()
        {
            List<SelectListItem> priorityList = new List<SelectListItem>();
            priorityList.Add(new SelectListItem { Text = "Normal", Value = "Normal" });
            priorityList.Add(new SelectListItem { Text = "Medium", Value = "Medium" });
            priorityList.Add(new SelectListItem { Text = "Urgent", Value = "Urgent" });

            SelectList priority = new SelectList(priorityList, "Value", "Text");
            ViewBag.priorityList = priority;
            //return View(db.Tasks.ToList());
            return View();
        }

        public ActionResult TaskList()
        {
            return PartialView(db.Tasks.ToList());
        }

        //
        // GET: /Task/Details/5

        public ActionResult Details(int id = 0)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            if (taskdb == null)
            {
                return HttpNotFound();
            }
            return View(taskdb);
        }

        //
        // GET: /Task/Create

        public ActionResult Create()
        {
           
            return PartialView();
        }

        //
        // POST: /Task/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskDB taskdb)
        {
            
            if (ModelState.IsValid)
            {
                db.Tasks.Add(taskdb);
                db.SaveChanges();
                return RedirectToAction("TaskList");
            }

            return RedirectToAction("TaskList");
        }

        //
        // GET: /Task/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            if (taskdb == null)
            {
                return HttpNotFound();
            }

            List<SelectListItem> priorityList = new List<SelectListItem>();
            priorityList.Add(new SelectListItem { Text = "Normal", Value = "Normal" });
            priorityList.Add(new SelectListItem { Text = "Medium", Value = "Medium" });
            priorityList.Add(new SelectListItem { Text = "Urgent", Value = "Urgent" });

            SelectList priority = new SelectList(priorityList, "Value", "Text");
            ViewBag.priorityList = priority;

            return PartialView(taskdb);
        }

        //
        // POST: /Task/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TaskDB taskdb)
        {
            if (!ModelState.IsValid)
            {
                var errors = GetErrorsFromModelState();
                return Json(new { success = false, errors = errors });
                //return PartialView("_Person", model);
            }
            if (ModelState.IsValid)
            {
                db.Entry(taskdb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("TaskList");
            }
            return RedirectToAction("TaskList");
        }

        private Dictionary<string, ModelErrorCollection> GetErrorsFromModelState() //IEnumerable<string>
        {
            //return ModelState.SelectMany(x => x.Value.Errors
            //    .Select(error => error.ErrorMessage));

            return ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0).ToDictionary(key => key, key => ModelState[key].Errors);
        }

        //
        // GET: /Task/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            if (taskdb == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("TaskList");
        }

        //
        // POST: /Task/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TaskDB taskdb = db.Tasks.Find(id);
            db.Tasks.Remove(taskdb);
            db.SaveChanges();
            return RedirectToAction("TaskList");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Chart()
        {
            return View(BarChart());
        }

        public JsonResult BarChart()
        {
            var chartData = db.Tasks.ToList();
            return Json(chartData, JsonRequestBehavior.AllowGet);
        }
    }
}